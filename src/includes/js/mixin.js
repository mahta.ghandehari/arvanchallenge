import axios from 'axios';

const mixin = {
    methods: {
        makeToastNotification: function (type, title, message) {
            const id = `my-toast-${this.$data.notifCounter++}`;
            const $closeButton = this.$createElement(
                'b-button',
                {
                    on: { click: () => this.$bvToast.hide(id) },
                },
                'x',
            );
            const $title = this.$createElement(
                'span',
                {
                    style: {
                        fontWeight: 'bold',
                    },
                },
                title,
            );
            const $message = this.$createElement('span', message);
            this.$bvToast.toast([$closeButton, $title, $message], {
                id: id,
                variant: type,
                autoHideDelay: 5000,
                noCloseButton: true,
            });
        },

        axiosApiCall: function (title, method, url, query, body) {
            return axios({
                method: method,
                url: process.env.BASE_URL + '/' + url,
                contentType: 'application/json; charset=utf-8',
                params: query,
                data: body,
                headers: {
                    Authorization: this.$cookie.get('auth') ? `Token ${JSON.parse(this.$cookie.get('auth')).token}` : '',
                },
            })
                .then((response) => {
                    console.log('response', response);
                    return method === 'get' ? response.data : response;
                })
                .catch((error) => {
                    const returnedError = [];
                    console.log('err', error.response.data);
                    if (error.response && error.response.data && error.response.data.errors) {
                        for (const [key, value] of Object.entries(error.response.data.errors)) {
                            returnedError.push(value.map((data) => `${key} ${data}`).join(','));
                        }
                    } else if (error.response && error.response.data && error.response.data.error) {
                        returnedError.push(error.response.data.error);
                    }
                    this.$root.makeToastNotification('danger', title ? title : 'Api call error ', returnedError.join(','));
                });
        },
    },
};
export default mixin;
