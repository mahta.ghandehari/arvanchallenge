export default function checkAuthentication(to, from, next, authCookieInfo) {
    // function to check if user has authority to visit the page
    if (to.matched.some((record) => record.meta.requiresAuth)) {
        if (!authCookieInfo) {
            next({ name: 'Login' });
        } else {
            next();
        }
    } else {
        next();
    }
}
