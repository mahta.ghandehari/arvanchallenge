import Vue from 'vue';
import VeeValidate, { Validator } from 'vee-validate';

Vue.use(VeeValidate, {
    events: '',
    classes: true,
    classNames: {
        valid: 'is-valid',
        invalid: 'is-invalid',
    },
    inject: true,
    // Important to name this something other than 'fields'
    fieldsBagName: 'veeFields',
    // This is not required but avoids possible naming conflicts
    errorBagName: 'veeErrors',
});

const dict = {
    custom: {
        loginPassword: {
            required: 'Required field',
        },
        name: {
            required: () => 'Your name is empty',
        },
    },
};

Validator.localize('en', dict);
