import AppLogin from '@/components/AppLogin.vue';
import AppRegister from '@/components/AppRegister.vue';
import CreateArticle from '@/components/Article/CreateArticle.vue';
import ArticleList from '@/components/Article/ArticleList.vue';

const routes = [
    {
        path: '/',
        redirect: '/article',
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/login',
        name: 'Login',
        component: AppLogin,
    },
    {
        path: '/register',
        name: 'Register',
        component: AppRegister,
    },
    /* Article page */
    {
        path: '/article/page/:id',
        name: 'ArticleList',
        component: ArticleList,
        // alias: '/article',
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/article',
        name: 'ArticleList',
        component: ArticleList,
        meta: {
            requiresAuth: true,
        },
    },
    /* Create Article */
    {
        path: '/article/create',
        name: 'CreateArticle',
        component: CreateArticle,
        meta: {
            requiresAuth: true,
        },
    },
    /* Edit Article */
    {
        path: '/article/edit/:slug',
        name: 'EditArticle',
        component: CreateArticle,
        meta: {
            requiresAuth: true,
        },
    },
];

export default routes;
