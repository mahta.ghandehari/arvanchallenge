// Import Vue
import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import VueCookie from 'vue-cookie';

//import Axios
import axios from 'axios';
import VueAxios from 'vue-axios';

// vee validation setup
import './includes/js/validation';
import mixin from '@/includes/js/mixin';

// Import Vue App, routes, store
import App from './App';
import routes from './routes';
import checkAuthentication from '@/includes/js/guards';

// bootstrap styles
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
// our global styles
import './includes/styles/_global.scss';

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueCookie);

// Configure router
const router = new VueRouter({
    routes,
    linkActiveClass: 'active',
    mode: 'history',
});

router.beforeEach((to, from, next) => {
    const authCookieInfo = Vue.cookie.get('auth');
    checkAuthentication(to, from, next, authCookieInfo);
});

new Vue({
    el: '#app',
    mixins: [mixin],
    data: function () {
        return {
            notifCounter: 0,
        };
    },
    render: (h) => h(App),
    router,
});
