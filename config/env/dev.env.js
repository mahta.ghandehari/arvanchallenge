module.exports = {
    NODE_ENV: 'development',

    BASE_URL: 'https://conduit.productionready.io/api',
};
